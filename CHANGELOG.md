## [1.0.14](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.13...v1.0.14) (2024-12-29)


### Bug Fixes

* **Dockerfile:** Update openDesk CLI to v2.7.1 ([61ece79](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/61ece796f96181005599c2b8506d0aeb7d6833fa))

## [1.0.13](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.12...v1.0.13) (2024-12-29)


### Bug Fixes

* **Dockerfile:** Update openDesk CLI to v2.7.0 ([8e2b422](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/8e2b42276325d395f69fd34c506e36db9a955d47))

## [1.0.12](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.11...v1.0.12) (2024-12-27)


### Bug Fixes

* **Dockefile:** Update to CI-Helm v1.2.0 and openDesk CLI v2.6.0 ([c392739](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/c392739142e84d4b062dc7d04a78ba5a65aaee1c))
* **Dockerfile:** Update nodejs to 22.11.0-r0 ([c1f965e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/c1f965e84c99c3886ccf519fd56db672f48b6928))

## [1.0.11](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.10...v1.0.11) (2024-12-24)


### Bug Fixes

* **Dockerfile:** Update openDesk CLI to v2.5.6 ([0ebb9a1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/0ebb9a1cde0c03f9cf703b046507874e9f78d6cb))

## [1.0.10](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.9...v1.0.10) (2024-12-24)


### Bug Fixes

* **Dockerfile:** Update openDesk CLI to v2.5.5 ([c60716a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/c60716abb12b34c5c0e2d82c53a3ad87705d7b95))

## [1.0.9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.8...v1.0.9) (2024-12-24)


### Bug Fixes

* **Dockerfile:** Update nodejs to 22.11-r1 ([c47ce5c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/c47ce5cc0078bbc31240aaedeec8f36b3da3211c))
* **Dockerfile:** Update openDesk CLI to v2.5.4 ([10ffd26](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/10ffd26e1829f7d2190ea0caee0f70032e61e788))

## [1.0.8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.7...v1.0.8) (2024-12-10)


### Bug Fixes

* **Dockerfile:** Update Kyverno to v1.13.2 and openDesk CLI to v2.5.3 ([8059614](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/80596146316833189b5697fe38fea9a3a3ec21eb))
* **Dockerfile:** Update nodejs to 22.11 ([a3abc93](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/a3abc93808077bd22a091686d2f00940b17ac3cd))

## [1.0.7](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.6...v1.0.7) (2024-10-13)


### Bug Fixes

* **Dockerfile:** Update OPENDESK_CI_CLI_VERSION to v2.5.2 ([5f69a53](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/5f69a53a2f34914347a52aa447bf6121bd3cdbf9))

## [1.0.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.5...v1.0.6) (2024-09-18)


### Bug Fixes

* **Dockerfile:** Update nodejs/npm ([1810251](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/1810251fe8de71a20399c7b733ca7f1ed76650ed))
* **Dockerfile:** Update openDesk CLI to v2.5.0 ([c1e22b5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/c1e22b5f4db1d28f031f34baeb4fe6b1697e4c92))

## [1.0.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.4...v1.0.5) (2024-06-09)


### Bug Fixes

* **Dockerfile:** Update opendesk ci cli ([e63c930](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/e63c93088d1675baf46df96572a1aacd0d5a68a6))

## [1.0.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.3...v1.0.4) (2024-02-20)


### Bug Fixes

* **Dockerfile:** Update nodejs to 20.11.1-r0 ([63dbfdf](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/63dbfdfd36c9bf63583d76efa62532aa1617b052))
* **Dockerfile:** Update openDesk CLI to v2.4.3 ([bb346e2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/bb346e26bc40c78bb77cab4808f07a3609f0e334))

## [1.0.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.2...v1.0.3) (2024-02-01)


### Bug Fixes

* **Dockerfile:** Update openDesk CLI v2.3.1 ([2a43212](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/2a43212c34b9c093e2f60ca3432913826d138c02))

## [1.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.1...v1.0.2) (2024-02-01)


### Bug Fixes

* **Dockerfile:** Update npm to 10.4.0 ([ffff208](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/ffff208460161d31708d0373aa53fc548b08eb20))
* **Dockerfile:** Update to Kyverno v1.11.4 and openDesk CLI v2.3.0 ([3ffe892](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/3ffe892fbfb48c31cf0fc3115cdc8834fdea0dee))

## [1.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/compare/v1.0.0...v1.0.1) (2024-01-21)


### Bug Fixes

* **Dockerfile:** Update openDesk CLI to v2.2.0 ([0ecbc9b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/0ecbc9bdd7ba6779d0602a83c17b401c73f00f72))

# 1.0.0 (2024-01-21)


### Features

* Initial commit ([28c8d68](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/commit/28c8d68e11ed8562837edae7ed0fcb6483c28991))
