# SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
FROM registry.opencode.de/bmi/opendesk/components/platform-development/images/helm:1.2.0@sha256:23366933500b48513c3f591710c8d67740fdbad8b6f57eaa26b069b48351a225

LABEL org.opencontainers.image.authors="Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH <hallo@zendis.de>" \
      org.opencontainers.image.documentation=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint/-/blob/main/README.md \
      org.opencontainers.image.source=https://gitlab.opencode.de/bmi/opendesk/components/platform-development/images/ci-lint \
      org.opencontainers.image.vendor="Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH <hallo@zendis.de>" \
      org.opencontainers.image.licenses=Apache-2.0 \
      org.opencontainers.image.base.name=registry.opencode.de/bmi/opendesk/components/platform-development/images/helm:1.2.0 \
      org.opencontainers.image.base.digest=sha256:23366933500b48513c3f591710c8d67740fdbad8b6f57eaa26b069b48351a225

WORKDIR /app

ARG KYVERNO_CLI_VERSION="v1.13.2"
ARG OPENDESK_CI_CLI_VERSION="v2.7.1"

RUN wget --quiet "https://github.com/kyverno/kyverno/releases/download/${KYVERNO_CLI_VERSION}/kyverno-cli_${KYVERNO_CLI_VERSION}_linux_x86_64.tar.gz" \
 && tar -xf kyverno-cli_${KYVERNO_CLI_VERSION}_linux_x86_64.tar.gz \
 && rm -rf kyverno-cli_${KYVERNO_CLI_VERSION}_linux_x86_64.tar.gz \
 && mv kyverno /usr/local/bin \
 && chmod +x /usr/local/bin/kyverno \
 && apk add --no-cache \
    nodejs=22.11.0-r0 \
    npm=10.9.1-r0 \
 && git clone --depth 1 --branch ${OPENDESK_CI_CLI_VERSION} https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli.git \
 && cd opendesk-ci-cli \
 && npm ci
