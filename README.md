<!--
SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# Kyverno CLI Image

This image creates a container image including tools for linting openDesk project.

## Content:
- [Helm](https://github.com/helm/helm)
- [Helmfile](https://github.com/helmfile/helmfile)
- [Kyverno](https://github.com/kyverno/kyverno)
- [openDesk CI CLI](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
